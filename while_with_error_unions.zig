const std = @import("std");
const testing = std.testing;

// when the else |x| form is used, x must be an error union
var numbers_left: u32 = undefined;

test "while error union capture" {
    var sum: u32 = 0;
    numbers_left = 3;

    while (eventuallyErrorSequence()) |value| {
        sum += value;
    } else |err| {
        testing.expectEqual(err, error.NoMoreNumbers);
    }
    testing.expectEqual(sum, 3);
}

fn eventuallyErrorSequence() anyerror!u32 {
    return if (numbers_left == 0) error.NoMoreNumbers else blk: {
        numbers_left -= 1;
        break :blk numbers_left;
    };
}
