const std = @import("std");
const debug = std.debug;

pub fn main() void {
    const Foo = struct {};
    debug.print("{s}\n", .{@typeName(Foo)});

    const Bar = List(i32);
    debug.print("{s}\n", .{@typeName(Bar)});

    debug.print("{s}\n", .{@typeName(struct {})});
}

fn List(comptime T: type) type {
    return struct {
        x: T,
    };
}
