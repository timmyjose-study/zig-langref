const std = @import("std");
const debug = std.debug;
const testing = std.testing;

// ABI aligned
const Point = struct {
    x: i32,
    y: i32,
};

// specific latout guaranteed
const Point2 = packed struct {
    x: i32,
    y: i32,
};

const p = Point{
    .x = 100,
    .y = 200,
};

var p2 = Point{
    .x = 200,
    .y = undefined,
};

// struct functions are namespaced functions
const Vec3 = struct {
    x: i32,
    y: i32,
    z: i32,

    pub fn init(x: i32, y: i32, z: i32) Vec3 {
        return Vec3{
            .x = x,
            .y = y,
            .z = z,
        };
    }

    // self here is simply a convention - there is no implicit `this`
    pub fn dot(self: *const Vec3, other: *const Vec3) i32 {
        return self.x * other.x + self.y * other.y + self.z * other.z;
    }
};

test "struct init" {
    const v1 = Vec3.init(10, 20, 30);
    const v2 = Vec3.init(-10, -20, -30);
    debug.print("v1.v2 = {d}\n", .{v1.dot(&v2)});
    debug.print("v1.v2 = {d}\n", .{Vec3.dot(&v1, &v2)});
}

// namespaced global variable
const Empty = struct {
    pub const PI = 3.14159;
};

test "namespaced global variables" {
    testing.expect(Empty.PI == 3.14159);
    testing.expect(@sizeOf(Empty) == 0);

    const does_nothing = Empty{};
}

fn setYBasedOnX(x: *i32, y: i32) void {
    const point = @fieldParentPtr(Point, "x", x);
    point.y = y;
}

test "fieldParentPtr" {
    var pp = Point{
        .x = 100,
        .y = undefined,
    };

    setYBasedOnX(&pp.x, 100);
    testing.expect(pp.y == 100);
}

const Foo = struct { a: i32 = 12345, b: i32 };

test "default field values" {
    const foo = Foo{ .b = 200 };
    testing.expect(foo.a == 12345);
    testing.expect(foo.b == 200);
}

const Bitfield = packed struct {
    a: u3,
    b: u3,
    c: u2,
};

test "pointer to non-byte-aligned field" {
    var bitfield = Bitfield{ .a = 1, .b = 2, .c = 3 };
    // testing.expect(bar(&bitfield.b) == 2);
    testing.expect(@ptrToInt(&bitfield.a) == @ptrToInt(&bitfield));
    testing.expect(@ptrToInt(&bitfield.b) == @ptrToInt(&bitfield));
    testing.expect(@ptrToInt(&bitfield.c) == @ptrToInt(&bitfield));

    debug.print("byte offset of a = {}, bit offset of a = {}\n", .{ @byteOffsetOf(Bitfield, "a"), @bitOffsetOf(Bitfield, "a") });
    debug.print("byte offset of b = {}, bit offset of a = {}\n", .{ @byteOffsetOf(Bitfield, "b"), @bitOffsetOf(Bitfield, "b") });
    debug.print("byte offset of c = {}, bit offset of a = {}\n", .{ @byteOffsetOf(Bitfield, "c"), @bitOffsetOf(Bitfield, "c") });
}

// this doesn't work since the pointer types are different
//fn bar(ptr: *const u3) u3 {
//    return ptr.*;
//}

test "alignment of struct fields" {
    const S = struct {
        a: u32 align(2),
        b: u16 align(64),
        c: u8 align(128),
    };

    var foo = S{ .a = 1, .b = 2, .c = 3 };
    testing.expectEqual(foo.a, 1);
    testing.expectEqual(foo.b, 2);
    testing.expectEqual(@TypeOf(&foo.a), *align(2) u32);
    testing.expectEqual(@TypeOf(&foo.b), *align(64) u16);
    testing.expectEqual(@alignOf(S), 128); // takes the largest alignment
}
