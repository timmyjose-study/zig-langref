const std = @import("std");
const debug = std.debug;
const testing = std.testing;

test "nested break" {
    var count: usize = 0;

    outer: for ([_]u32{ 1, 2, 3, 4, 5 }) |_| {
        for ([_]u32{ 1, 2, 3, 4, 5 }) |_| {
            count += 1;
            break :outer;
        }
    }
    testing.expectEqual(count, 1);
}

test "nested continue" {
    var count: usize = 0;

    outer: for ([_]u32{ 1, 2, 3, 4, 5 }) |_| {
        for ([_]u32{ 1, 2, 3, 4, 5 }) |_| {
            count += 1;
            continue :outer;
        }
    }
    testing.expectEqual(count, 5);
}
