const std = @import("std");
const debug = std.debug;
const mem = std.mem;
const testing = std.testing;

test "string literals and unicode code point" {
    const bytes = "hello";
    testing.expect(@TypeOf(bytes) == *const [5:0]u8);
    testing.expect(mem.eql(u8, &[_]u8{ 'h', 'e', 'l', 'l', 'o' }, bytes));
    testing.expect(bytes.len == 5);
    testing.expect(bytes[0] == 'h');
    testing.expect(bytes[2] == 'l');
}

test "multiline string literals" {
    const program =
        \\#include <iostream>
        \\
        \\ int main() {
        \\  std::cout << "Hello, world" << std::endl;
        \\  return 0;
        \\}
    ;

    debug.print("{s}\n", .{program});
}
