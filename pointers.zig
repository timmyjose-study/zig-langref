const std = @import("std");
const debug = std.debug;
const testing = std.testing;

// Zig has two kinds of pointers:
//
// 1. single-item pointer: *T
// 2. multi-item pointer: [*]T
//
// These types are closely related to arrays and slices:
//
// 3. *[N]T, and
// 4. []T
//

test "address of syntax" {
    const x: i32 = 42;
    const ptr_x = &x;
    testing.expect(ptr_x.* == x);
    testing.expect(@TypeOf(ptr_x) == *const i32);

    var y: i32 = 42;
    var ptr_y = &y;
    testing.expect(@TypeOf(ptr_y) == *i32);
    const ptr_yy = &y;
    testing.expect(@TypeOf(ptr_yy) == *i32); // constness is determined by the target object

    ptr_yy.* += 100;
    testing.expect(y == 142);
    testing.expect(ptr_y.* == 142);
    testing.expect(ptr_yy.* == 142);
}

test "pointer array access" {
    var array = [_]i32{ 1, -1, 2, 0, 3, 11 };
    const ptr_2 = &array[2];
    testing.expect(@TypeOf(ptr_2) == *i32);
    testing.expect(ptr_2.* == 2);
    ptr_2.* += 100;

    for (array) |e| {
        debug.print("{} ", .{e});
    }
    debug.print("\n", .{});
}

test "pointer slicing" {
    var array = [_]u32{ 1, 2, 3, 4, 5 };
    const slice = array[2..5];
    testing.expect(slice.len == 3);
    testing.expect(slice[1] == array[3]);
    slice[1] += 100;
    testing.expect(array[3] == 104);
}

test "comptime pointers" {
    // pointers work at comptime as well, given a known memory layout
    comptime {
        var foo: i32 = 100;
        const ptr_foo = &foo;
        ptr_foo.* += 100;
        testing.expect(ptr_foo.* == 200);
        testing.expect(foo == 200);
    }
}

test "intToPtr and ptrToInt" {
    const ptr = @intToPtr(*align(1) i32, 0xdeadbeef);
    debug.print("alignment of ptr = {}\n", .{@alignOf(@TypeOf(ptr))});
    const addr = @ptrToInt(ptr);
    debug.print("addr = {x}\n", .{addr});
}

test "comptime @intToPtr" {
    // Zig can work with intToPtr and ptrToInt at comptime, so long as the
    // pointer is never dereferenced during comptime.
    comptime {
        const ptr = @intToPtr(*align(1) i32, 0xdeadbeef);
        const addr = @ptrToInt(ptr);
        testing.expect(addr == 0xdeadbeef);
    }
}
