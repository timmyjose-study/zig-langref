const std = @import("std");
const debug = std.debug;
const testing = std.testing;

// noreturn is the type of the following:
// - break
// - continue
// - while(true) {}
// - unreachable
// - return
// noreturn is compatible with every other type in places where it matters - if and switch.

fn foo(condition: bool, b: u32) void {
    const a = if (condition) b else return;
    @panic("do something with a!");
}

test "noreturn" {
    foo(false, 1);
    //foo(true, 1);
}
