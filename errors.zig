const std = @import("std");
const debug = std.debug;
const testing = std.testing;

const FileOpenError = error{
    AccessDenied,
    OutOfMemory,
    FileNotFound,
};

const AllocationError = error{OutOfMemory};

// a superset error type is basically the bigger error set which also contains all the
// subset's error variants.
test "coerce subset to superset for error types" {
    const err = foo(AllocationError.OutOfMemory);
    testing.expectEqual(err, FileOpenError.OutOfMemory);
}

fn foo(err: AllocationError) FileOpenError {
    return err;
}

// this needs a cast
test "coerce superset to subset for error types" {
    const err = bar(FileOpenError.OutOfMemory);
    testing.expectEqual(err, AllocationError.OutOfMemory);
}

fn bar(err: FileOpenError) AllocationError {
    return @errSetCast(AllocationError, err);
}

test "reflection on error unions types" {
    var quux: anyerror!i32 = undefined;

    quux = 12345;
    quux = error.SomeError;
    comptime testing.expectEqual(@typeInfo(@TypeOf(quux)).ErrorUnion.payload, i32);
    comptime testing.expectEqual(@typeInfo(@TypeOf(quux)).ErrorUnion.error_set, anyerror);

    var foobar: FileOpenError![]const u8 = FileOpenError.AccessDenied;
    comptime testing.expectEqual(@typeInfo(@TypeOf(foobar)).ErrorUnion.error_set, FileOpenError);
}
