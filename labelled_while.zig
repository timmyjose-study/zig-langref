const std = @import("std");
const testing = std.testing;

test "nested break" {
    var i: usize = 0;
    var j: usize = 1;

    outer: while (i < 100) : (i += 1) {
        while (j < 100) : (j *= 2) {
            if (i * j > 100) break :outer;
        }
    }
}

test "nested continue" {
    var i: usize = 0;
    var j: usize = 1;

    outer: while (i < 100) : (i += 1) {
        while (j < 100) : (j *= 2) {
            if (i * j < 100) continue :outer;
        }
    }
}
