const std = @import("std");
const debug = std.debug;
const testing = std.testing;

pub extern "c" fn printf(fmt: [*:0]const u8, ...) c_int;

test "sentinel-terminated pointers" {
    _ = printf("%s\n", "Hello, world");

    const msg = "Hello again, world!";
    const non_null_terminated_string: [msg.len]u8 = msg.*;
    //_ = printf(&non_null_terminated_string);
}
