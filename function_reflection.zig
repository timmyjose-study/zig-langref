const std = @import("std");
const debug = std.debug;
const testing = std.testing;

fn foo() align(16) void {}

test "reflection" {
    testing.expectEqual(@typeInfo(@TypeOf(foo)).Fn.return_type, void);
    testing.expectEqual(@typeInfo(@TypeOf(foo)).Fn.is_var_args, false);
    testing.expectEqual(@typeInfo(@TypeOf(foo)).Fn.alignment, 16);
    debug.print("typeInfo for foo = {s}\n", .{@typeInfo(@TypeOf(foo))});
}

fn add10(x: anytype) @TypeOf(x) {
    return x + 10;
}

// this does not work
//test "reflection for a function return type inferred function" {
//    debug.print("{}\n", .{@typeInfo(@TypeOf(add10))});
//}
