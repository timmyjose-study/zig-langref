const std = @import("std");
const mem = std.mem;
const debug = std.debug;
const testing = std.testing;

test "peer resolve int widening" {
    var a: i8 = 1;
    var b: i32 = 2;
    var c = a + b;
    testing.expectEqual(@TypeOf(c), i32);
}

test "peer resolve arrays of different sizes to const slice" {
    var arr1 = [_]i32{ 1, 2, 3 };
    var arr2 = [_]i32{ 11, 12, 13, 14, 15 };

    var slice1: []const i32 = &arr1;
    var slice2: []const i32 = &arr2;
}

test "peer resolve array and const slice" {
    var arr: [5]u8 = "hello".*;
    const slice = arr[0..];
    testing.expect(mem.eql(u8, &arr, slice));
}

test "peer type resolution ?T and T" {
    var x1: i32 = 100;
    var x2: ?i32 = x1;
    testing.expectEqual(x2.?, x1);
}
