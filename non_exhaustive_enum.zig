const std = @import("std");
const testing = std.testing;

const Number = enum(u2) { // size must be specified
    zero,
    one,
    two,
    _,
};

test "switch on non-exhaustive enum" {
    const foo = Number.two;
    const foo_res = switch (foo) {
        .zero => false,
        .one => false,
        .two => true,
        _ => false, // when using this, all stated literals must be handled
    };
    testing.expect(foo_res);

    const bar = Number.zero;
    const bar_res = switch (bar) {
        .zero => true,
        else => false, // see the difference with using else instead of _?
    };
    testing.expect(bar_res);
}
