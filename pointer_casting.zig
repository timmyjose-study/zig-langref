const std = @import("std");
const builtin = std.builtin;
const debug = std.debug;
const testing = std.testing;

test "pointer casting" {
    const bytes align(@alignOf(u32)) = [_]u8{ 0x12, 0x13, 0x14, 0x15 };
    const u32_ptr = @ptrCast(*const u32, &bytes);
    const u32_value = @bitCast(u32, bytes);
    const u32_another_value = std.mem.bytesAsSlice(u32, bytes[0..])[0];

    if (builtin.endian == .Little) {
        testing.expect(u32_ptr.* == 0x15141312);
        testing.expect(u32_value == 0x15141312);
        testing.expect(u32_another_value == 0x15141312);
    } else {
        testing.expect(u32_ptr.* == 0x12131415);
        testing.expect(u32_value == 0x15141312);
        testing.expect(u32_another_value == 0x15141312);
    }
}

test "pointer child type" {
    testing.expect(@typeInfo(*i32).Pointer.child == i32);

    const x: i32 = 100;
    const ptr_i32 align(@alignOf(i32)) = &x;
    testing.expect(@typeInfo(@TypeOf(ptr_i32)).Pointer.child == i32);

    const bytes align(@alignOf(i8)) = [_]u8{ 0x12, 0x13, 0x14, 0x15 };
    testing.expect(@typeInfo(@TypeOf(@ptrCast(*const u32, &bytes))).Pointer.child == u32);
}

test "variable alignment" {
    var x: i32 = 42;
    const align_of_x = @alignOf(@TypeOf(x));
    testing.expect(@TypeOf(&x) == *align(align_of_x) i32);

    if (std.Target.current.cpu.arch == .x86_64) {
        testing.expect(@typeInfo(*i32).Pointer.alignment == 4);
    }
}
