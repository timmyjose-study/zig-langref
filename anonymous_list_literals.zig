const std = @import("std");
const mem = std.mem;
const debug = std.debug;
const testing = std.testing;

test "anonymous list literals" {
    var array: [5]u32 = .{ 1, 2, 3, 4, 5 };
    testing.expect(array.len == 5);

    for (array) |e, idx| {
        debug.print("{d}\n", .{e});
        testing.expect(e == @intCast(i32, idx + 1));
    }
}

// in this case, the anonymous list literal turns into an anonymous struct literal
// with numbered field names.
fn dump(obj: anytype) void {
    testing.expect(obj.@"0" == 12345);
    testing.expect(obj.@"1" == 2.78128);
    testing.expect(obj.@"2" == true);
    testing.expect(mem.eql(u8, obj.@"3", "hi"));
    testing.expect(obj.@"4" == 'x');
}

test "fully anonymous list literals" {
    dump(.{ @as(u32, 12345), @as(f64, 2.78128), true, "hi", 'x' });
}

const Point = struct {
    x: i32,
    y: i32,
    z: i32,
};

fn dumpStruct(s: anytype) void {
    debug.print("{}, {}, {}\n", .{ s.@"x", s.@"y", s.@"z" });
}

test "anonymous struct literals" {
    dumpStruct(.{
        .x = 1,
        .y = 2,
        .z = 3,
    });
}
