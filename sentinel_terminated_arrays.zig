const std = @import("std");
const testing = std.testing;

// [N:x] T
test "null terminated arrays" {
    const array = [_:0]u32{ 1, 2, 3, 4, 5 };
    testing.expect(array.len == 5);
    testing.expect(array[1] == 2);
    testing.expect(array[array.len] == 0);

    const message = "hello, world";
    testing.expect(message.len == 12);
    testing.expect(message[message.len] == 0);

    // this works too!
    const foo = [_:'x']u8{ 'h', 'e', 'l', 'l', 'o' };
    testing.expect(foo.len == 5);
    testing.expect(foo[foo.len] == 'x');
}
