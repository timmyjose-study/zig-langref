const std = @import("std");
const mem = std.mem;
const testing = std.testing;

const Point = struct { x: i32, y: i32 };

test "anonymous struct literals" {
    const p = .{ .x = 100, .y = -200 };
    testing.expectEqual(p.x, 100);
    testing.expectEqual(p.y, -200);
}

fn dump(args: anytype) void {
    testing.expectEqual(args.x, 100);
    testing.expectEqual(args.y, false);
    testing.expect(mem.eql(u8, "hello", args.z));
}

test "fully anonymous struct literals" {
    dump(.{ .x = 100, .y = false, .z = "hello" });
}
