//! This module provides functions to defining a Timestamp 
//! (This is a module level doc comment).
const std = @import("std");

/// A structure for storing a timestamp, with nanosecond precision (this
/// is a multiline doc comment)
const Timestamp = struct {
    /// the number of seconds since the Unix Epoch
    seconds: i64,
    /// the number of nanoseconds past the second
    nanos: u32,

    pub fn unixEpoch() Timestamp {
        return Timestamp{
            .seconds = 0,
            .nanos = 0,
        };
    }
};

test "comments" {
    // This is a normal commment in Zig
    const x = true;
    std.testing.expect(x);
}

test "unix epoch" {
    const epoch = Timestamp.unixEpoch();
    std.testing.expect(epoch.seconds == 0);
    std.testing.expect(epoch.nanos == 0);
}
