const std = @import("std");
const testing = std.testing;

const Point = struct {
    x: i32,
    y: i32,

    pub fn init(x: i32, y: i32) Point {
        return Point{ .x = x, .y = y };
    }

    // Zig has a bit of magic here in the sense that objects (such as Point) here, might be passed by value
    // or by reference depending on its optimising schemes. Arguments are immutable in Zig. I think it is
    // better to be explicit about it rather than relying on magic.
    pub fn add(self: Point, other: *const Point) Point {
        return Point{
            .x = self.x + other.x,
            .y = self.y + other.y,
        };
    }
};

test "Point" {
    const p1 = Point.init(100, 200);
    const p2 = Point.init(-100, -200);
    const p3 = p1.add(&p2);
    testing.expectEqual(p3, Point.init(0, 0));
}
