const std = @import("std");
const debug = std.debug;

// Zig cannot infer the size here
const matrix = [3][3]f32{ [_]f32{ 1.2, 2.3, 3.4 }, [_]f32{ -2.3, -3.2, -0.22 }, [_]f32{ 0.09, 1312.232, -12212.22 } };

test "multi-dimensional arrays" {
    for (matrix) |row| {
        for (row) |e| {
            debug.print("{} ", .{e});
        }
        debug.print("\n", .{});
    }
}
