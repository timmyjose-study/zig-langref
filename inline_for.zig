const std = @import("std");
const debug = std.debug;
const testing = std.testing;

test "inline for" {
    const nums = [_]i32{ 2, 4, 6 };
    var sum: usize = 0;
    inline for (nums) |n| {
        const T = switch (n) {
            2 => f32,
            4 => bool,
            6 => []const u8,
            else => unreachable,
        };
        sum += typeNameLength(T);
    } else {
        testing.expectEqual(sum, 17);
    }
}

fn typeNameLength(comptime T: type) usize {
    return @typeName(T).len;
}
