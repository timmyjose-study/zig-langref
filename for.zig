const std = @import("std");
const debug = std.debug;
const testing = std.testing;

test "for basics" {
    var array = [_]u32{ 1, 2, 3, 4, 5 };
    var sum: u32 = 0;

    for (array) |e| {
        if (@rem(e, 2) == 0) continue;
        sum += e;
    }
    testing.expectEqual(sum, 9);

    sum = 0;
    for (array[2..4]) |e| {
        sum += e;
    }
    testing.expectEqual(sum, 7);

    for (array) |e, idx| {
        debug.print("element at index {} = {}\n", .{ idx, e });
    }
}

test "for reference" {
    var items = [_]u32{ 1, 2, 3, 4, 5 };
    for (items) |*e| {
        e.* += 100;
    } else {
        testing.expectEqual(items[0], 101);
    }
}

// for, like switch, while, and if, is also an expression
test "for else" {
    var items = [_]?i32{ 1, 2, null, 3, 4, null, 5 };
    var sum: i32 = 0;

    for (items) |e| {
        if (e != null) {
            sum += e.?;
        }
    } else {
        testing.expectEqual(sum, 15);
    }
}
