const std = @import("std");

pub fn main() void {
    // integers
    const one_plus_one: i32 = 1 + 1;
    std.debug.print("1 + 2 = {}\n", .{one_plus_one});

    // floats
    const seven_div_three = 7.0 / 3.0;
    std.debug.print("7.0 / 3.0 = {}\n", .{seven_div_three});

    // boolean
    const a = false;
    const b = true;
    std.debug.print("{} and {} = {}\n{} or {} = {}\n!{} = {}\n!{} = {}\n", .{ a, b, a and b, a, b, a or b, a, !a, b, !b });

    // optionals
    var optional_value: ?[]const u8 = null;
    std.debug.assert(optional_value == null);
    std.debug.print("optional 1\ntype = {s}, value = {s}\n", .{ @typeName(@TypeOf(optional_value)), optional_value });

    optional_value = "hello";
    std.debug.assert(std.mem.eql(u8, "hello", optional_value.?));
    std.debug.print("optional 2\ntype = {s}, value = {s}\n", .{ @typeName(@TypeOf(optional_value)), optional_value });

    // error union
    var number_or_error: anyerror!i32 = error.ArgNotFound;
    std.debug.print("error union 1\ntype = {s}, value = {}\n", .{ @typeName(@TypeOf(number_or_error)), number_or_error });

    number_or_error = 42;
    std.debug.print("error union 1\ntype = {s}, value = {}\n", .{ @typeName(@TypeOf(number_or_error)), number_or_error });
}
