const std = @import("std");
const testing = std.testing;

const Color = enum { auto, off, on };

test "enum literals" {
    const color1: Color = .auto;
    const color2 = .auto;
    testing.expect(color1 == color2);
}

test "switch using enum literals" {
    const color = .on;
    const result = switch (color) {
        .on => true,
        else => false, // this prong is required when using enum literals
    };
    testing.expect(result);
}
