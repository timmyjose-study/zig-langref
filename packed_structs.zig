const std = @import("std");
const testing = std.testing;

const Full = packed struct {
    number: u16,
};

const Divided = packed struct {
    half1: u8,
    quarter3: u4,
    quarter4: u4,
};

test "package structs" {
    const full = Full{ .number = 0x1234 };
    doTheTest(&full);
    comptime doTheTest(&full);
}

fn doTheTest(full: *const Full) void {
    testing.expect(@sizeOf(Full) == 2);
    testing.expect(@sizeOf(Divided) == 2);
    const divided = @bitCast(Divided, full.*);

    switch (std.builtin.endian) {
        .Big => {
            testing.expect(divided.half1 == 0x12);
            testing.expect(divided.quarter3 == 0x3);
            testing.expect(divided.quarter4 == 0x4);
        },
        .Little => {
            testing.expect(divided.half1 == 0x34);
            testing.expect(divided.quarter3 == 0x2);
            testing.expect(divided.quarter4 == 0x1);
        },
    }
}

const Bitfield = struct {
    a: u3,
    b: u3,
    c: u2,
};

test "get the address of a non-byte-aligned field" {
    const b = Bitfield{ .a = 1, .b = 2, .c = 3 };
    const ptr_bb = &b.b;
    testing.expect(ptr_bb.* == 2);
}
