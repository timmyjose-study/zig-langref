const std = @import("std");
const mem = std.mem;
const debug = std.debug;
const testing = std.testing;

fn dump(arg: anytype) void {
    debug.print("{}\n", .{arg});
    testing.expectEqual(100, arg[0]);
    testing.expectEqual(false, arg[1]);
    testing.expect(mem.eql(u8, "hello", arg[2]));

    inline for (arg) |v, idx| {
        if (idx < 3) continue;
        testing.expect(v);
    }
}

test "tuples using anonymous struct literals with no named fields" {
    dump(.{ 100, false, "hello" } ++ .{true} ** 5);
}
