const std = @import("std");
const testing = std.testing;

var numbers_left: u32 = undefined;

// when the while (foo) |bar| form is used, bar must be an optional type
test "while null capture" {
    var sum1: u32 = 0;
    numbers_left = 3;

    while (eventuallyNullSequence()) |value| {
        sum1 += value;
    }
    testing.expectEqual(sum1, 3);

    var sum2: u32 = 0;
    numbers_left = 3;
    while (eventuallyNullSequence()) |value| {
        sum2 += value;
    } else {
        testing.expectEqual(sum2, 3);
    }
}

fn eventuallyNullSequence() ?u32 {
    return if (numbers_left == 0) null else blk: {
        numbers_left -= 1;
        break :blk numbers_left;
    };
}
