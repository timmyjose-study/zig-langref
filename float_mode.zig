const std = @import("std");
const debug = std.debug;

extern fn floatStrict(f64) f64;
extern fn floatOptimized(f64) f64;

pub fn main() void {
    const x = 0.001;
    debug.print("strict = {}\n", .{floatStrict(x)});
    debug.print("optimized = {}\n", .{floatOptimized(x)});
}
