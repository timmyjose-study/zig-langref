const std = @import("std");
const testing = std.testing;

test "const and var" {
    const foo = 12345;

    var bar: i32 = 1;
    bar += 1;
    testing.expect(foo == 12345);
    testing.expect(bar == 2);

    // this works too
    comptime var baz = 99;
    comptime {
        baz += 1;
    }
    testing.expect(baz == 100);
}

test "undefined" {
    var undef: i32 = undefined;
    undef = 99;
    testing.expect(undef == 99);
}
