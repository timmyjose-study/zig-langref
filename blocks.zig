const std = @import("std");
const debug = std.debug;
const testing = std.testing;

test "basic blocks" {
    // this is fine because the lexical scopes are siblings, not parent-child,
    // and shadowing is not allowed.
    {
        var x: i32 = 100;
        debug.print("{}\n", .{x});
    }

    {
        var x: i32 = 200;
        debug.print("{}\n", .{x});
    }
}

// blocks are essentially expressions, but we need to use the break
// keyword to extract values out of a block
test "labelled break from a block" {
    const res = blk: {
        const x = 100;
        const y = 200;
        break :blk x + y;
    };
    testing.expectEqual(res, 300);

    const another_res = {
        const x = 100;
        const y = 200;
    };
    debug.print("{}\n", .{another_res}); // this will be void.

    var x: i32 = 99;
    var y = calc_y: {
        x += 1;
        break :calc_y x;
    };
    testing.expectEqual(x, y);
}
