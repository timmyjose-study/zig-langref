const std = @import("std");
const debug = std.debug;

test "integers" {
    const decimal_int = 10;
    const binary_int = 0b1010;
    const octal_int = 0o12;
    const hex_int = 0x0a;
    const large_decimal_int = 1_000_000;

    debug.print("{d}, {b}, {o}, {x}, {d}\n", .{ decimal_int, binary_int, octal_int, hex_int, large_decimal_int });
}
