const std = @import("std");
const debug = std.debug;
const testing = std.testing;

test "alignment" {
    var foo: u8 align(4) = 100;
    testing.expect(@TypeOf(&foo) == *align(4) u8);
    testing.expect(@typeInfo(@TypeOf(&foo)).Pointer.alignment == 4);

    const as_pointer_to_array: *[1]u8 = &foo;
    const as_slice: []u8 = as_pointer_to_array;
    testing.expect(@TypeOf(as_slice) == []align(4) u8);
}

fn noop1() align(1) void {}

fn noop2() align(8) void {}

fn derp() align(@sizeOf(usize) * 2) i32 {
    return 12345;
}

test "function alignment" {
    testing.expect(@TypeOf(noop1) == fn () align(1) void);
    testing.expect(@TypeOf(noop2) == fn () align(8) void);
    testing.expect(derp() == 12345);
    if (std.Target.current.cpu.arch == .x86_64) {
        testing.expect(@TypeOf(derp) == fn () align(16) i32);
    }
}
