const std = @import("std");
const debug = std.debug;
const math = std.math;

test "floats" {
    const floating_point = 123.0e+77;
    const another_floating_point = 123.0;
    const yet_another = 123.0E+77;

    const pos_inf = math.inf(f32);
    const neg_inf = -math.inf(f32);
    const nan = math.nan(f32);

    debug.print("{}, {}, {}, {}, {}, {}\n", .{ floating_point, another_floating_point, yet_another, pos_inf, neg_inf, nan });
}

test "floating-point operations" {
    // see float.zig and float_mode.zig
}
