const std = @import("std");
const testing = std.testing;

// generics in Zig are done using comptime functions
fn LinkedList(comptime T: type) type {
    return struct {
        pub const Node = struct {
            prev: ?*Node,
            next: ?*Node,
            data: T,
        };

        first: ?*Node,
        last: ?*Node,
        len: usize,
    };
}

test "linked list" {
    var list = LinkedList(i32){
        .first = null,
        .last = null,
        .len = 0,
    };

    const ListOfInts = LinkedList(i32);

    var node = ListOfInts.Node{
        .prev = null,
        .next = null,
        .data = 1,
    };

    var list2 = ListOfInts{
        .first = &node,
        .last = &node,
        .len = 1,
    };

    testing.expect(list2.first.?.data == 1);
    testing.expect(list2.last.?.data == 1);
    testing.expect(list2.len == 1);
}
