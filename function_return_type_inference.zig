const std = @import("std");
const testing = std.testing;

// anytype here is basically any type that ie coercion compatible with 42 (comptime_int)
fn addFortyTwo(x: anytype) @TypeOf(x) {
    return x + 42;
}

test "function return type inference" {
    testing.expectEqual(addFortyTwo(1), 43);
    //_ = addFortyTwo("hello");
    testing.expectEqual(@TypeOf(addFortyTwo(1)), comptime_int);
    testing.expectEqual(@TypeOf(addFortyTwo(@as(i32, 1))), i32);
    testing.expectEqual(@TypeOf(addFortyTwo(@as(u32, 1))), u32);
}
