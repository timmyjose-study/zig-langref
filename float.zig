const big = @as(f64, 1 << 40);

export fn floatStrict(x: f64) f64 {
    return x + big - big;
}

export fn floatOptimized(x: f64) f64 {
    @setFloatMode(.Optimized);
    return x + big - big;
}
