const std = @import("std");
const math = std.math;
const debug = std.debug;
const testing = std.testing;

fn parseU64(buf: []const u8, radix: u8) !u64 {
    var x: u64 = 0;

    for (buf) |c| {
        const digit = charToDigit(c);

        if (digit >= radix) {
            return error.InvalidChar;
        }

        if (@mulWithOverflow(u64, x, radix, &x)) {
            return error.Overflow;
        }

        if (@addWithOverflow(u64, x, digit, &x)) {
            return error.Overflow;
        }
    }

    return x;
}

fn charToDigit(c: u8) u8 {
    return switch (c) {
        '0'...'9' => c - '0',
        'A'...'Z' => c - 'A' + 10,
        'a'...'z' => c - 'a' + 10,
        else => math.maxInt(u8),
    };
}

test "parseU64" {
    const result = try parseU64("12345", 10);
    testing.expectEqual(result, 12345);
}

// error handling in Zig can be classified into the following approaches:
//
// 1. provide a default value in case of error. (use catch)
// 2. if error, propagate the same error. (use catch |err| or its equivalent try)
// 3. forcefully unwrap the value (use catch unreachable)
// 4. handle each possible error. (use if and switch)

test "provide a default value using catch" {
    const result = parseU64("12345", 10) catch 99;
    testing.expectEqual(result, 12345);

    const another_result = parseU64("deadbeef", 10) catch 99;
    testing.expectEqual(another_result, 99);
}

fn foo(buf: []const u8, radix: u8, expected_result: u64) !void {
    const result = parseU64(buf, radix) catch |err| return err;
    testing.expectEqual(result, expected_result);

    // this is the shortcut version of the above
    const another_result = try parseU64(buf, radix);
    testing.expectEqual(another_result, expected_result);
}

test "use result if successful else propagate the error using try" {
    foo("12345", 10, 12345) catch {};
}

test "unwrap a known valid result forcefully using catch unreachable" {
    const result = parseU64("12345", 10) catch unreachable;
    testing.expectEqual(result, 12345);
}

test "handle all errors manually using if and switch on error" {
    if (parseU64("12345", 10)) |result| {
        testing.expectEqual(result, 12345);
    } else |err| switch (err) {
        error.InvalidChar => debug.print("invalid character for radix in input\n", .{}),
        error.Overflow => debug.print("value is too big to fit in the radix\n", .{}),
    }
}
