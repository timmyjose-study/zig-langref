const std = @import("std");
const testing = std.testing;

// this is basically loop-unrolling - only works for comptime
test "inline while loops at comptime" {
    comptime var i = 0;
    comptime var sum = 0;

    inline while (i < 3) : (i += 1) {
        const T = switch (i) {
            0 => bool,
            1 => i32,
            2 => []const u8,
            else => unreachable,
        };

        sum += typeNameLength(T);
    }

    comptime testing.expectEqual(sum, 17);
}

fn typeNameLength(comptime T: type) comptime_int {
    return @typeName(T).len;
}
