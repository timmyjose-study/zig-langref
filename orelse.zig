const std = @import("std");
const testing = std.testing;

fn getNumber(b: bool) ?i32 {
    return if (b) 42 else null;
}

// orelse is a simple way of providing a default value for an optional type
test "orelse" {
    const true_res = getNumber(true) orelse 0;
    testing.expectEqual(true_res, 42);

    const false_res = getNumber(false) orelse 0;
    testing.expectEqual(false_res, 0);
}
