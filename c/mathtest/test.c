#include "mathtest.h"
#include <stdio.h>

int main(int argc, char *argv[])
{
  int x, y;
  scanf("%d%d", &x, &y);
  printf("%d + %d = %d\n", x, y, add(x, y));

  return 0;
}