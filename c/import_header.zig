const c = @cImport({
    @cInclude("stdio.h");
    @cInclude("string.h");
});

pub fn main() void {
    const msg: *const [5:0]u8 = "hello";
    _ = c.printf("\"%s\" has a length of %d\n", msg, c.strlen(msg));
}
