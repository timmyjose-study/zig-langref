const std = @import("std");
const debug = std.debug;

extern "c" fn sub(i32, i32) i32;

pub fn main() void {
    debug.print("{} - {} = {}\n", .{ 10, 2, sub(10, 2) });
}
