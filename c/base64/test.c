#include <stdio.h>
#include <string.h>

extern size_t decode_base_64(char*, size_t, const char*, size_t);

int main(int argc, char *argv[])
{
    const char* encoded = "SGVsbG8sIHdvcmxkLg==";
    char buf[200];
    size_t len = decode_base_64(buf, 200, encoded, strlen(encoded));
    buf[len] = 0;
    puts(buf);

    return 0;
}


