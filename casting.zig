const std = @import("std");
const mem = std.mem;
const debug = std.debug;
const testing = std.testing;

// type coercion happens when a type is expected, but a different, compatible type is provided
test "type coercion - variable declaration" {
    var a: u8 = 1;
    var b: u16 = a;
    testing.expectEqual(b, 1);
}

test "type coercion - function call" {
    var a: u8 = 42;
    foo(a);
}

fn foo(x: u32) void {
    testing.expectEqual(x, 42);
}

test "type coercion - @as builtin" {
    var a: u8 = 1;
    var b: u64 = @as(u64, a);
    testing.expectEqual(b, 1);
}

// type coercion - stricter qualification
// non-const -> const
// non-volatile -> volatile
// bigger alignment -> smaller alignment
// error sets -> supersets

test "type coercion - const qualification" {
    var a: i32 = 1;
    var b: *i32 = &a;
    bar(b);
}

fn bar(ptr: *const i32) void {
    testing.expectEqual(ptr.*, 1);
}

test "type coercion - pointers to const optioanl pointers" {
    var a: u8 = 1;
    var b: *u8 = &a;
    baz(b);
}

fn baz(ptr: ?*const u8) void {
    testing.expectEqual(ptr.?.*, 1);
}

test "integer and float widening" {
    var ii1: u8 = 42;
    var ii2: u16 = ii1;
    var ii3: u32 = ii2;
    var ii4: u64 = ii3;
    var ii5: u128 = ii4;
    testing.expectEqual(ii5, ii1);

    var ff1: f16 = 1.2345;
    var ff2: f32 = ff1;
    var ff3: f64 = ff2;
    var ff4: f128 = ff3;
    testing.expectEqual(ff4, ff1);
}

test "implicit unsigned integer to signed integer" {
    var a: u8 = 10;
    var b: i16 = a;
    testing.expectEqual(b, a);
}

test "[N]T to []const T" {
    var x1: []const u8 = "hello, world";
    var x2: []const u8 = &[_]u8{ 'h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd' };
    testing.expect(mem.eql(u8, x1, x2));

    var y: []const f32 = &[_]f32{ 1.2, 2.3, 3.4 };
    testing.expectEqual(@as(f32, 1.2), y[0]);
}

test "[N]T to E![]const T" {
    var x1: anyerror![]const u8 = "hello";
    var x2: anyerror![]const u8 = &[_]u8{ 'h', 'e', 'l', 'l', 'o' };
    testing.expect(mem.eql(u8, try x1, try x2));
}

test "[N]T to ?[]const T" {
    var x1: ?[]const u8 = "hello";
    var x2: ?[]const u8 = &[_]u8{ 'h', 'e', 'l', 'l', 'o' };
    testing.expect(mem.eql(u8, x1.?, x2.?));
}

test "*[N]T to []T" {
    var x1: [5]u8 = "hello".*;
    var x2: []u8 = &x1;
    testing.expect(mem.eql(u8, x2, "hello"));
}

test "*[N]T to [*]T" {
    var x1: [5]u8 = "hello".*;
    var x2: [*]u8 = &x1;
    testing.expectEqual(x2[4], 'o');
}

test "*[N]T to ?[*]T" {
    var buf: [5]u8 = "hello".*;
    var x: ?[*]u8 = &buf;
    testing.expectEqual(x.?[4], 'o');
}

test "*T to *[1]T" {
    var x: i32 = 42;
    var ptr: *[1]i32 = &x;
    testing.expectEqual(ptr.*[0], 42);
}

test "coerce to optionals" {
    var x: ?i32 = null;
    testing.expect(x == null);

    var y: ?i32 = 12345;
    testing.expectEqual(y.?, 12345);
}

test "coerce to optionals wrapped in error union" {
    const x: anyerror!?i32 = 42;
    testing.expectEqual((try x).?, 42);
    const y: anyerror!?i32 = null;
    testing.expect((try y) == null);
}

test "coerce to error unions" {
    const x: anyerror![]const u8 = "hello";
    testing.expect(mem.eql(u8, try x, "hello"));
    const y: anyerror![]const u8 = error.SomethingAwful;
    testing.expectEqual(y, error.SomethingAwful);
    testing.expectError(error.SomethingAwful, y);
}

test "coercing larger integral value to smaller one when value is comptime known to fit" {
    const x1: u64 = 100;
    const x2: u8 = x1;
    testing.expectEqual(x1, x2);
}

test "coercion between unions and enums when they are comptime known to be compatible" {
    const E = enum { one, two, three };
    const U = union(E) { one: i32, two: bool, three };

    var e_1 = E.one;
    var u_1 = U{ .one = 42 };
    testing.expectEqual(e_1, u_1);

    var e_2 = E.two;
    var u_2 = U{ .two = false };
    testing.expectEqual(e_2, u_2);

    var e_3 = E.three;
    var u_3 = U.three;
    testing.expectEqual(e_3, u_3);
}

test "Zero-bit types can be coerced to single-item pointers" {
    var x1: void = {};
    var x2: *void = x1;
}

test "undefine can be cast to any type" {
    var x1: []const u8 = undefined;
    var x2: i32 = undefined;
    var x3: anyerror!?bool = undefined;
    var x4: void = undefined;
}
