const std = @import("std");
const debug = std.debug;
const testing = std.testing;

test "the usual while loop" {
    var counter: usize = 0;

    while (counter < 10) {
        counter += 1;
    }

    testing.expectEqual(counter, 10);
}

test "while break" {
    var counter: usize = 0;

    while (true) {
        counter += 1;
        if (counter > 10) break;
    }
    testing.expectEqual(counter, 11);
}

test "while continue" {
    var number: i32 = 0;

    while (number < 100) {
        number += 1;
        if (@rem(number, 2) == 0) continue;
        debug.print("{} ", .{number});
    }
    debug.print("\n", .{});
}

test "while loop continue expressions" {
    var number: usize = 0;
    while (number < 10) : (number += 1) {
        debug.print("{} ", .{number});
    }
    debug.print("\n", .{});
}

// while is an expression, just like switch
test "while loop continue expresssions, more complicated" {
    var i: usize = 1;
    var j: usize = 2;

    while (i * j <= 2000) : ({
        i += 1;
        j *= 2;
    }) {
        const ij = i * j;
        testing.expect(ij <= 2000);
    }
}

fn rangeHasNumber(start: i32, end: i32, number: i32) bool {
    var curr: i32 = start;

    return while (curr <= end) : (curr += 1) {
        if (curr == number) break true;
    } else false;
}

test "while else" {
    testing.expect(rangeHasNumber(0, 10, 5));
    testing.expect(!rangeHasNumber(0, 10, 15));
}
