const std = @import("std");
const debug = std.debug;
const testing = std.testing;

test "optional type reflection" {
    var foo: ?i32 = null;
    testing.expect(foo == null);

    foo = 12345;
    testing.expectEqual(foo.?, 12345);
    testing.expectEqual(@typeInfo(@TypeOf(foo)).Optional.child, i32);
}

test "optional pointers" {
    var ptr: ?*i32 = null;
    var x: i32 = 42;
    ptr = &x;
    testing.expectEqual(ptr.?.*, x);
    testing.expectEqual(@sizeOf(@TypeOf(ptr)), @sizeOf(*i32));
}
