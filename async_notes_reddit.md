Source: https://www.reddit.com/r/Zig/comments/lwnlbz/trying_to_understand_the_colorless_async_handling/gpkekh5/


I think a major issue with discussions of Zig's async/await is that `io_mode` always shows up in the conversation, leading people to think that async/await is tied to it or, worse yet, *is* it.

That definitely isn't the case though, so I hope that this will both answer your question as well as help other people understand Zig's async/await a bit better.

## Async

`async` is a keyword that calls a function in an **asynchronous context**.

Let's break that down using the following code:

```
    const std = @import("std");
    
    fn foo() void {
        std.debug.print("Hello\n", .{});
    }
    
    pub fn main() void {
        foo();
        _ = async foo();
    }
```

### Part 1: "calls a function"

In this example:

1. `foo()` is executed, handing control of the program from `main` to `foo`.
2. `foo` executes `std.debug.print()` causing "Hello\\n" to be printed.
3. `foo` finishes and so control is handed from `foo` back to `main`.
4. `main` then executes `async foo()`.
5. `async foo()` hands control over to `foo`.
6. `foo` executes `std.debug.print()` causing "Hello\\n" to be printed.

If you're currently thinking that `foo()` and `async foo()` seem to be identical in their behavior (ignoring that `async foo()` returned something), then you'd be right! Both calls go *into* `foo`.

`async foo()` doesn't execute `foo` in the background, immediately return a "future," or anything like that.

### Part 2: "asynchronous context"

Okay, so that's the first part of `async`'s definition. Now let's talk about the "asynchronous context" portion.

Consider the following:

```
    fn foo() void {
        suspend;          // suspension point
    }
    
    pub fn main() void {
        // foo();         //compile error
        _ = async foo();
    }
```

Here, `foo` contains something called a **suspension point**. In this case, the suspension point is created by the `suspend` keyword. I'll get into suspension points later, so for now just note that `foo` has one.

In any case, we can learn what "asynchronous context" means by looking at the program's behavior:

* If `foo()` wasn't a comment and was actually executed, it would emit a compile error because normal function calls do not allow for suspension points in the called function.
    * Technically this isn't exactly true. See the Colorless Functions section for more information.
* In contrast, functions that are called in an asynchronous context (i.e., with `async`) *do* allow for suspension points in the called function, and so `async foo()` compiles and runs without issue.

So, at this point, you can think of "calling a function in an asynchronous context" as "we call a function and suspension points are allowed in that function." Pretty simple, right?

# Suspension Points

But what exactly are suspension points? And why do we need a different calling syntax for functions that contain them?

Well, in short, suspension points are points at which a function is suspended (not very helpful, I know). More specifically, suspending a function involves:

1. "Pausing" the function's execution.
2. Saving information about the function (into something called an **async frame**) so that it may be "resumed" later on.  
    * To resume a suspended function, the `resume` keyword is used on the suspended function's async frame.
3. Handing control of the program back to whichever function called the now-suspended function.
    * During this, the suspended function's async frame is passed along to the caller and that frame is what is returned from `async <function call>`.

The most common ways to create suspension points are `suspend` and `await`.

## Await

Hang on a second, `await` is a suspension point? **Yes,** `await` **is a suspension point**.

That means that `await` behaves just as I've described: it pauses the current function, saves function information into a frame, and then hands control over to whichever function called the now-suspended function.

**It does not resume suspended functions.**

For example, consider the following:

```
    const std = @import("std");
    
    fn foo() u8 {
        suspend;
        return 1;  // never reached
    }
    
    fn asyncMain() void {
        var frame = async foo();
        const one = await frame;
        std.debug.print("Hello\n", .{});  // never reached
    }
    
    pub fn main() void {
        _ = async asyncMain();
    }
```

We'll go through this step by step:

1. `main` can't have a suspension point for technical reasons, so we call a wrapper function, `asyncMain`, in an asynchronous context, handing control over to `asyncMain`.
2. `asyncMain` calls `foo` in an asynchronous context, handing control over to `foo`.
3. `foo` suspends by executing `suspend`. That is, `foo` is paused and hands control (and an async frame) back to the function that called it: `asyncMain`.
4. `asyncMain` regains control and `async foo()` finishes executing due to `foo` suspending, and returns `foo`'s async frame, which `asyncMain` then stores in the `frame` variable.
    * If `foo` hadn't executed `suspend` (i.e., if `foo` just returned `1`), `async foo()` would still have finished its execution because Zig places an implicit suspension point before the return of functions that are called in an asynchronous context.
4. `asyncMain` moves on and executes `await frame`, suspending itself and handing control back to its caller: `main`.
5. `main` regains control and `async asyncMain()` finishes executing due to `asyncMain` suspending.
6. `main` continues on but there's nothing left to do so the program exits.

Note that `return 1` and `std.debug.print("Hello\n", .{})` are never executed:

* `foo` was never resumed after being suspended so `return 1` was never reached.
* `asyncMain` was never resumed after being suspended so `std.debug.print("Hello\n", .{})` was never reached.

### Return Value Coordination

At this point, you might be wondering why there's `await` if it seemingly does the same thing as `suspend`? Well, that's because `await` does more than just suspending the current function: it coordinates return values.

Consider the following, where instead of just suspending, `foo` stores it's async frame in a global variable, which `main` uses to resume `foo`'s execution later on:

```
    const std = @import("std");
    
    var foo_frame: anyframe = undefined;
    
    fn foo() u8 {
        suspend foo_frame = @frame();
        return 1;
    }
    
    fn asyncMain() void {
        var frame = async foo();
        const one = await frame;
        std.debug.print("Hello\n", .{});
    }
    
    pub fn main() void {
        _ = async asyncMain();
        resume foo_frame;
    }
```
Here, we go through the same steps from before, but with a few differences:

* When `foo` begins its execution, it stores its frame into `foo_frame` just before suspending.
* When `main` regains control after `asyncMain` suspends via `await`, `main` continues on and executes `resume foo_frame`, giving control back to `foo`, which then continues from where it left off and executes `return 1`.

But where does control go to now? Back to `main`? Or to `asyncMain`, which is still `await`ing on a frame for `foo`? This is where `await` comes in.

If a suspended function returns, and its frame is being awaited on, then control of the program is given to the awaiter (i.e., `asyncMain`). `await` returns the value returned by the awaited function. So, in this case, `await frame` returns with `1` and then that value is assigned to the constant `one`. After that, `asyncMain` continues and prints "Hello\\n" to the screen.

## I/O Mode

Note that I have not mentioned `io_mode` once. That's because `io_mode` has no effect on `async` or `await`.

So to answer your second question: Does the compiler ignore `async` and `await` keywords when in blocking mode? The answer is no. Everything behaves exactly as I have described.

The purpose of `io_mode` is to remove the division seen in a lot of language ecosystems where synchronous and asynchronous libraries are completely separate from one another. The idea is that library writers can check `io_mode` to see if it's `.blocking` or `.evented`, and then perform its services synchronously or asynchronously depending on whichever the library user desires. Personally, I think this global switch is too inflexible but that's another discussion.

## Colorless Functions

Now, if you set `io_mode = .evented`, then the library code you use will more than likely go down some async code path, creating suspension points. But normal function calls can't have suspension points in the called function, so `myAsyncFunction()` would be a compile error, right?

Well, it depends. Except for a few cases, calling an async function outside an asynchronous context (i.e., `foo()` instead of `async foo()`) implicitly adds `await async` to the call signature. That is, `myAsyncFunction()` becomes `await async myAsyncFunction()`; and hopefully by now you can understand what `await async myAsyncFunction()` would do: executes `myAsyncFunction` in an asynchronous context and then suspends the calling function.

The big point here is that not only does Zig allow async and non-async library code to live together, it also allows library users to express concurrency (i.e., use async/await) even if they're not currently taking advantage of it since it will behave correctly. Do note though that it doesn't work the other way around: writing synchronous code that calls async functions which don't offer synchronous support isn't going to end well for you because your code will be suspending everywhere, and if you're writing synchronous code, you're probably not handling that properly.

## Conclusion

Okay, that was a lot. But I hope I was able to answer your question. If you have any other questions, feel free to ask!